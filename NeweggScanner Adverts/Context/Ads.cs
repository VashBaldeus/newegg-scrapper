using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace NeweggScanner_Adverts.Context
{
    public partial class Ads : DbContext
    {
        public Ads()
            : base("name=Ads")
        {
        }

        public Ads(string conn)
            : base(conn)
        {
        }

        public virtual DbSet<HtmlLink> HtmlLinks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
