﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExtensionMethods;
using NeweggScanner_Adverts.Context;

namespace NeweggScanner_Adverts
{
    public class MainViewModel : OnPropertyChange
    {
        #region Constructor

        public MainViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            OnLoadCommand = new RelayCommand(OnLoadExecute);

            ButtonsCommand = new RelayCommand(ButtonsExecute);
        }

        #endregion

        #region ICommands

        public ICommand OnLoadCommand { get; set; }

        public void OnLoadExecute(object obj)
        {
            try
            {
                InitAdverts();
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand ButtonsCommand { get; set; }

        public void ButtonsExecute(object obj)
        {
            try
            {
                var ads = Ads();

                switch (obj.ToString())
                {
                    case "add":
                        if (SelectedHtml.ID > 0)
                            return;

                        ads.Entry(SelectedHtml).State = EntityState.Added;
                        ads.SaveChanges();

                        Adverts.Add(SelectedHtml);

                        SelectedHtml = new HtmlLink();
                        break;

                    case "save":

                        if (SelectedHtml.ID == 0)
                            return;

                        if (ads.HtmlLinks.Any(w => w.ID == SelectedHtml.ID && w.Clicks != SelectedHtml.Clicks))
                        {
                            var ad = ads.HtmlLinks.FirstOrDefault(w =>
                                w.ID == SelectedHtml.ID && w.Clicks != SelectedHtml.Clicks);

                            ad.AdvertName = SelectedHtml.AdvertName;
                            ad.Html = SelectedHtml.Html;
                            ad.ImageSource = SelectedHtml.ImageSource;
                            ad.HiddenAd = SelectedHtml.HiddenAd;

                            ads.Entry(ad).State = EntityState.Modified;
                            ads.SaveChanges();
                        }
                        else
                        {
                            ads.Entry(SelectedHtml).State = EntityState.Modified;
                            ads.SaveChanges();
                        }
                        break;

                    case "del":
                        if (SelectedHtml.ID == 0)
                            return;

                        if (MessageBox.Show($"Are you sure you want to delete [{SelectedHtml.AdvertName}]?",
                                "Deletion Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                            MessageBoxResult.No)
                            return;

                        ads.Entry(SelectedHtml).State = EntityState.Deleted;
                        ads.SaveChanges();

                        Adverts.Remove(SelectedHtml);

                        SelectedHtml = new HtmlLink();
                        break;

                    case "clear":
                        SelectedHtml = new HtmlLink();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        #endregion

        #region Public Properties

        private ObservableCollection<HtmlLink> _adverts = new ObservableCollection<HtmlLink>();

        public ObservableCollection<HtmlLink> Adverts
        {
            get => _adverts;
            set { _adverts = value; OnPropertyChanged(); }
        }

        private HtmlLink _selectedHtml = new HtmlLink();

        public HtmlLink SelectedHtml
        {
            get => _selectedHtml;
            set { _selectedHtml = value; OnPropertyChanged(); }
        }

        #endregion

        #region Private Properties

        private Ads Ads()
        {
            var g =
                "u1SYB9y99JmlTlWTS5ophIoO6b12P4Dkle5xQXvNEb7UPvjHCGbSe9bXFP4SNNY3eps+mEWWnmBJzIs/58x/ns4l5d68lmifhMy0sfCPT8BYHdl2PeTpxo8kjpisa8bx";

            return new Ads(
                $"data source=51.91.123.112;initial catalog=Scrapper;persist security info=True;user id=sa;password={g.Decrypt()};MultipleActiveResultSets=True;App=EntityFramework");
        }

        private void InitAdverts()
        {
            using (var db = Ads())
            {
                Adverts = new ObservableCollection<HtmlLink>(db.HtmlLinks);
            }
        }

        #endregion

        #region Public Methods



        #endregion

        #region Private Methods



        #endregion
    }
}
