﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NeweggScrapper.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void RunBot_OnChecked(object sender, RoutedEventArgs e)
        {
            //RunBot.Content = RunBot.IsChecked == true ? "STOP" : "RUN";
        }

        private void ClearBot_OnClick(object sender, RoutedEventArgs e)
        {
            CmbPresets.SelectedItem = null;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var btn = e.Source as Button;

            if (!btn.CommandParameter.ToString().StartsWith("http"))
                return;

            Process.Start($"{btn.CommandParameter}");
        }

        //private void CmbPresets_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    TglLinkLock.IsChecked = true;
        //}
    }
}
