﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.IconPacks;

namespace NeweggScrapper.Views
{
    /// <summary>
    /// Interaction logic for NotifyMeView.xaml
    /// </summary>
    public partial class NotifyMeView : UserControl
    {
        public NotifyMeView()
        {
            InitializeComponent();
        }

        private void TglPlayTune_OnChecked(object sender, RoutedEventArgs e)
        {
            TglPlayTune.Content =
                TglPlayTune.IsChecked == true
                    ? new PackIconEntypo { Kind = PackIconEntypoKind.ControllerPaus }
                    : new PackIconEntypo { Kind = PackIconEntypoKind.ControllerPlay };
        }

        private void BtnClearTelegram_OnClick(object sender, RoutedEventArgs e)
        {
            CmbTelegram.SelectedIndex = -1;
        }

        private void BtnClearDiscord_OnClick(object sender, RoutedEventArgs e)
        {
            CmbDiscord.SelectedIndex = -1;
        }
    }
}
