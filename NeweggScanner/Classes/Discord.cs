﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using ExtensionMethods;
using NeweggScrapper.Models;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace NeweggScrapper.Classes
{
    public class Discord
    {
        public static void SendDiscord(string webHooksToken, ScrapperResult item, ObservableCollection<string> roles = null)
        {
            string token = webHooksToken;
            WebRequest request = (HttpWebRequest)WebRequest.Create(token);
            request.ContentType = "application/json";
            request.Method = "POST";

            var tags = string.Empty;

            if (roles != null && roles.Count > 0)
            {
                tags = roles.Aggregate(string.Empty, (cur, i) => cur += $"<@&{i}> ");
            }

            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(new
                {
                    //username = "Spidey Bot",
                    embeds = new[]
                    {
                        new
                        {
                            title = $"WOOHOO! Stock found! :smirk: [{DateTime.UtcNow.ToLocalTime():dd/MMM/yyyy HH:mm:ss}]",

                            fields = new object[]
                            {
                                new{ name = "Product Pagelink", value = $"[**CLICK**]({item.Link})", inline = true},
                                new{ name = "Add To Cart", value = $"[**CLICK**]({item.AddToCart})", inline = true},
                                new{ name = "Brand", value = item.Brand , inline = true},
                                new{ name = "Price", value = item.Price , inline = true},
                                new{ name = "Description", value = item.Desc , inline = false}
                            },

                            color = "9697064"
                        }
                    },

                    content = tags
                });
                sw.Write(json);
            }

            // ReSharper disable once UnusedVariable
            var responnd = (HttpWebResponse)request.GetResponse();
        }
    }

    public static class Telegram
    {
        public static async void SendMessage(string key, string channel, string message)
        {
            try
            {
                var telegram = new TelegramBotClient(key);

                await telegram.SendTextMessageAsync(channel, message, ParseMode.MarkdownV2);
            }
            catch (Exception ex)
            {
                Message.Log(ex, string.Empty, MessageShow.No);
            }
        }
    }
    //public class Discord
    //{
    //    public static void SendDiscord(string webHooksToken, ScrapperResult item, ObservableCollection<string> roles = null)
    //    {
    //        string token = webHooksToken;
    //        WebRequest request = (HttpWebRequest)WebRequest.Create(token);
    //        request.ContentType = "application/json";
    //        request.Method = "POST";

    //        var tags = string.Empty;

    //        if (roles != null && roles.Count > 0)
    //        {
    //            tags = roles.Aggregate(string.Empty, (cur, i) => cur += $"<@&{i}> ");
    //        }

    //        using (var sw = new StreamWriter(request.GetRequestStream()))
    //        {
    //            string json = JsonConvert.SerializeObject(new
    //            {
    //                //username = "Spidey Bot",
    //                embeds = new[]
    //                {
    //                    new
    //                    {
    //                        title = $"WOOHOO! Stock found! :smirk: [{DateTime.UtcNow.ToLocalTime():dd/MMM/yyyy HH:mm:ss}]",

    //                        fields = new object[]
    //                        {
    //                            new{ name = "Product Pagelink", value = $"[**CLICK**]({item.Link})", inline = true},
    //                            new{ name = "Add To Cart", value = $"[**CLICK**]({item.AddToCart})", inline = true},
    //                            new{ name = "Brand", value = item.Brand , inline = true},
    //                            new{ name = "Price", value = item.Price , inline = true},
    //                            new{ name = "Description", value = item.Desc , inline = false}
    //                        },

    //                        color = "9697064"
    //                    }
    //                },

    //                content = tags
    //            });
    //            sw.Write(json);
    //        }

    //        // ReSharper disable once UnusedVariable
    //        var responnd = (HttpWebResponse)request.GetResponse();
    //    }
    //}

    //public static class Telegram
    //{
    //    public static async void SendMessage(string key, string channel,  string message)
    //    {
    //        try
    //        {
    //            var telegram = new TelegramBotClient(key);

    //            await telegram.SendTextMessageAsync(channel, message);
    //        }
    //        catch (Exception ex)
    //        {
    //            Message.Log(ex, string.Empty, MessageShow.No);
    //        }
    //    }
    //}
}