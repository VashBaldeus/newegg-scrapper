﻿using System;
using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading;
using System.Windows;

namespace NeweggScrapper.Classes
{
    public class Email
    {
        public Email(MainViewModel mvm)
        {
            Mvm = mvm;
        }

        private MainViewModel Mvm { get; }

        public void SendEmail(string to, string results)
        {
            try
            {
                var service = GmailServiceInit();
                InitMailStructAndSend(to, results, service);
            }
            catch (Exception ex)
            {
                Mvm.WriteDebug(ex);
            }
        }

        private void InitMailStructAndSend(string to, string results, GmailService service)
        {

            try
            {
                var plainText = new[]
                {
                    $"To: {to},\r\n",
                    "Subject: [Newegg] Product Results\r\n",
                    "Content-Type: text/html; charset=us-ascii\r\n\r\n",
                    $"{results}"
                };

                var newMsg = new Google.Apis.Gmail.v1.Data.Message
                    {Raw = Base64UrlEncode(plainText.Aggregate(string.Empty, (cur, i) => cur += i))};

                service.Users.Messages.Send(newMsg, "me").Execute();
            }
            catch (Exception e)
            {
                MessageBox.Show("Please provide at least one mail address\nor Uncheck \"Notify Me\" check box");
                Mvm.WriteDebug(e);
            }
        }

        private GmailService GmailServiceInit()
        {
            var credPath = "token.json";
            var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = "709269274693-2v01odgc9ub51j9o0p4kgpqa50ttm2et.apps.googleusercontent.com",
                    ClientSecret = "7M9RNCOTdogZyerISyZpJXcI"
                },
                new[]
                {
                    GmailService.Scope.GmailSend,
                },
                "user",
                CancellationToken.None,
                new FileDataStore(credPath, true)).Result;

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString()
            });
            return service;
        }

        private string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            var msg = System.Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
            return msg;
        }
    }
}
