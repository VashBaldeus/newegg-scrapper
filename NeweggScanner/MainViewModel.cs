﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using AutoUpdaterDotNET;
using ExtensionMethods;
using Microsoft.Win32;
using NeweggScrapper.Context;
using NeweggScrapper.Models;
using NeweggScrapper.Views;

namespace NeweggScrapper
{

    public class MainViewModel : OnPropertyChange
    {
        #region Constructor

        public MainViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            OnLoadCommand = new RelayCommand(OnLoadExecute);

            MenuCommand = new RelayCommand(MenuExecute);

            ScrapperCommand = new RelayCommand(ScrapperExecute);

            NotifyCommand = new RelayCommand(NotifyExecute);

            BlaclistCommand = new RelayCommand(BlaclistExecute);

            KeywordCommand = new RelayCommand(KeywordExecute);

            LinkCommand = new RelayCommand(LinkExecute);

            UpdateActiveStateCommand = new RelayCommand(UpdateActiveStateExecute);
        }

        #endregion

        #region ICommands

        public ICommand OnLoadCommand { get; set; }

        public void OnLoadExecute(object obj)
        {
            try
            {
                CheckUpdate();

                InitAutoUpdateTimer();

                DisplayContent = new MainView {DataContext = this};

                InitPresets();

                InitMediaPlayer();

                DatabaseConnectionTest();

                RegisterActiveClient();

                InitAdvertTimer(new TimeSpan(0, 0, 0, 30));
            }
            catch (Exception ex)
            {
                WriteDebug(ex);
            }
        }

        public ICommand MenuCommand { get; set; }

        public void MenuExecute(object obj)
        {
            try
            {
                if ((obj.ToString() != "notify" || obj.ToString() != "blacklist")
                    && !string.IsNullOrEmpty(SelectedScrapper.ID)
                    && !SelectedScrapper.PresetState)
                {
                    Xml.Serialize<ObservableCollection<ScrapperPreset>>(@"presets.cfg", Scrappers);
                }

                switch (obj.ToString())
                {
                    case "home":
                        DisplayContent = new MainView(){DataContext = this};
                        break;

                    case "notify":
                        DisplayContent = new NotifyMeView(){DataContext = this};
                        break;

                    case "blacklist":
                        DisplayContent = new BlacklistDictionaryView() { DataContext = this };
                        break;

                    case "debug":
                        DisplayContent = new DebugView(){DataContext = this};
                        break;
                }
            }
            catch (Exception ex)
            {
                WriteDebug(ex);
            }
        }

        public ICommand ScrapperCommand { get; set; }

        public void ScrapperExecute(object obj)
        {
            try
            {
                switch (obj.ToString())
                {
                    case "run":
                        //check if max searchers reached.
                        if (Scrappers.Count(c => c.PresetState) > 8)
                        {
                            SelectedScrapper.PresetState = false;
                            throw new Exception("You have reached the limit of simultaneous searches available. (limit being 8)\nNewEgg Limitation, otherwise they ban IP for 24 hours.");
                        }

                        CurrentlyRunning = Scrappers.Count(c => c.PresetState);

                        //run button pressed, switched to stop, kill worker.
                        if (SelectedScrapper.Worker.IsBusy && !SelectedScrapper.PresetState)
                        {
                            SelectedScrapper.Worker.CancelAsync();
                            return;
                        }

                        SelectedScrapper.Worker = new BackgroundWorker
                        {
                            WorkerSupportsCancellation = true
                        };

                        SelectedScrapper.Worker.DoWork += delegate
                        {
                            SelectedScrapper.InitScrapper(this);
                        };

                        SelectedScrapper.Worker.RunWorkerAsync();
                        break;

                    case "save":
                        SavePresets();
                        //if (string.IsNullOrEmpty(SelectedScrapper.ID))
                        //{
                        //    SelectedScrapper.ID = Guid.NewGuid().ToString();

                        //    Scrappers.Add(SelectedScrapper);
                        //}

                        //Xml.Serialize<ObservableCollection<ScrapperPreset>>(@"presets.cfg", Scrappers);
                        break;

                    case "delete":
                        //verify user wants to delete.
                        if (MessageBox.Show($"Are you sure you want to delete \"{SelectedScrapper.Name}\"?",
                            "Confirm Deletion", MessageBoxButton.YesNo,
                            MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                            return;

                        Scrappers.Remove(SelectedScrapper);

                        Xml.Serialize<ObservableCollection<ScrapperPreset>>(@"presets.cfg", Scrappers);

                        SelectedScrapper = new ScrapperPreset();
                        break;

                    case "clear":
                        SelectedScrapper = new ScrapperPreset();
                        break;

                    case "clone":
                        var ss = SelectedScrapper;

                        var clone = new ScrapperPreset
                        {
                            Settings = new ScrapperSettings
                            {
                                NotifySound = ss.Settings.NotifySound,
                                NotifyEmail = ss.Settings.NotifyEmail,
                                NotifyDiscord = ss.Settings.NotifyDiscord,
                                NotifyTelegram = ss.Settings.NotifyTelegram,
                                Sound = ss.Settings.Sound,
                                //TODO Telegram & Discord clone is not relevant, double check if so.
                                //Discord = new Discord
                                //{
                                //    WebHook = ss.Settings.Discord.WebHook,
                                //    MessageInterval = ss.Settings.Discord.MessageInterval,
                                //    RoleIds = new ObservableCollection<string>(ss.Settings.Discord.RoleIds)
                                //},

                                //Telegram = new Models.Telegram
                                //{
                                //    Key = ss.Settings.Telegram.Key,
                                //    Channel = ss.Settings.Telegram.Channel,
                                //    MessageInterval = ss.Settings.Telegram.MessageInterval
                                //}
                            },
                            Loop = ss.Loop,
                            LoopInterval = ss.LoopInterval
                        };

                        SelectedScrapper = null;

                        SelectedScrapper = clone;
                        break;
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand NotifyCommand { get; set; }

        public void NotifyExecute(object obj)
        {
            try
            {
                switch (obj.ToString())
                {
                    case "clearmail":
                        //verify user wants to delete.
                        if (MessageBox.Show($"Are you sure you want to clear email list?",
                            "Confirm Deletion", MessageBoxButton.YesNo,
                            MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                            return;

                        SelectedScrapper.Settings.Emails.Clear();
                        break;

                    case "addmail":
                        SelectedScrapper.Settings.Emails.Add(SelectedScrapper.Settings.Mail);
                        SelectedScrapper.Settings.Mail = string.Empty;
                        break;

                    case "sound":
                        var dialog = new OpenFileDialog();

                        dialog.SoundTypes();

                        if (dialog.ShowDialog() != true)
                            return;

                        SelectedScrapper.Settings.Sound = dialog.FileName;
                        break;

                    case "soundplay":
                        if(!SelectedScrapper.Settings.PlayerState)
                        {
                            _player.Stop();
                            return;
                        }

                        if (!string.IsNullOrEmpty(SelectedScrapper.Settings.Sound) && File.Exists(SelectedScrapper.Settings.Sound))
                        {
                            _player.Open(new Uri(SelectedScrapper.Settings.Sound));

                            _player.Play();
                        }
                        else if (File.Exists($@"{Directory.GetCurrentDirectory()}\notifysound.mp3"))
                        {
                            _player.Open(new Uri($@"{Directory.GetCurrentDirectory()}\notifysound.mp3"));

                            _player.Play();
                        }
                        break;

                    case "soundclear":
                        SelectedScrapper.Settings.Sound = string.Empty;
                        break;

                    case "discordrole":
                        SelectedScrapper.Settings.Discord.RoleIds.Add(SelectedScrapper.Settings.Discord.Role);
                        SelectedScrapper.Settings.Discord.Role = string.Empty;
                        break;

                    case "discordroleclear":
                        //verify user wants to delete.
                        if (MessageBox.Show("Are you sure you want to clear discord role list?",
                            "Confirm Deletion", MessageBoxButton.YesNo,
                            MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                            return;

                        SelectedScrapper.Settings.Discord.RoleIds.Clear();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand BlaclistCommand { get; set; }

        public void BlaclistExecute(object obj)
        {
            try
            {
                switch (obj.ToString())
                {
                    case "add":
                        SelectedScrapper.Settings.SelectedBlacklist.Id = Guid.NewGuid().ToString();

                        SelectedScrapper.Settings.BlacklistDictionary.Add(SelectedScrapper.Settings.SelectedBlacklist);

                        SelectedScrapper.Settings.SelectedBlacklist = new BlacklistWord();
                        break;

                    //case "save":
                    //    SavePresets();
                    //    break;

                    case "delete":
                        //verify user wants to delete.
                        if (MessageBox.Show($"Are you sure you want to delete \"{SelectedScrapper.Settings.SelectedBlacklist.Word}\"?",
                            "Confirm Deletion", MessageBoxButton.YesNo,
                            MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                            return;

                        SelectedScrapper.Settings.BlacklistDictionary.Remove(SelectedScrapper.Settings.SelectedBlacklist);

                        SavePresets();

                        SelectedScrapper.Settings.SelectedBlacklist = new BlacklistWord();
                        break;

                    case "clear":
                        SelectedScrapper.Settings.SelectedBlacklist = new BlacklistWord();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand KeywordCommand { get; set; }

        public void KeywordExecute(object obj)
        {
            try
            {
                if(obj == null || string.IsNullOrEmpty(obj.ToString()))
                    return;

                switch (obj.ToString().Split(';')[0])
                {
                    case "discord":
                        switch (obj.ToString().Split(';')[1])
                        {
                            case "add":
                                SelectedScrapper.Settings.Discord.Id = Guid.NewGuid().ToString();

                                //check if same name but different GUID exists in keywords, if true, do not allow add/save.
                                if (IsDuplicateKeyword(Duplicate.Discord))
                                {
                                    SelectedScrapper.Settings.Discord.Id = string.Empty;
                                    throw new Exception("Another Keyword exists with same word, you can not have duplicates!");
                                }

                                SelectedScrapper.Settings.Discords.Add(SelectedScrapper.Settings.Discord);

                                SelectedScrapper.Settings.Discord = new Discord();
                                break;

                            case "delete":
                                if (string.IsNullOrEmpty(SelectedScrapper.Settings.Discord.Id))
                                    return;

                                //verify user wants to delete.
                                if (MessageBox.Show($"Are you sure you want to delete \"{SelectedScrapper.Settings.Discord.KeyWord}\"?",
                                    "Confirm Deletion", MessageBoxButton.YesNo,
                                    MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                    return;

                                SelectedScrapper.Settings.Discords.Remove(SelectedScrapper.Settings.Discord);

                                SelectedScrapper.Settings.Discord = new Discord();
                                break;

                            case "clear":
                                SelectedScrapper.Settings.Discord = new Discord();
                                break;
                        }
                        break;

                    case "telegram":
                        switch (obj.ToString().Split(';')[1])
                        {
                            case "add":
                                SelectedScrapper.Settings.Telegram.Id = Guid.NewGuid().ToString();

                                //check if same name but different GUID exists in keywords, if true, do not allow add/save.
                                if (IsDuplicateKeyword(Duplicate.Telegram))
                                {
                                    SelectedScrapper.Settings.Telegram.Id = string.Empty;
                                    throw new Exception("Another Keyword exists with same word, you can not have duplicates!");
                                }

                                SelectedScrapper.Settings.Telegrams.Add(SelectedScrapper.Settings.Telegram);

                                SelectedScrapper.Settings.Telegram = new Models.Telegram();
                                break;

                            case "delete":
                                if (string.IsNullOrEmpty(SelectedScrapper.Settings.Telegram.Id))
                                    return;

                                //verify user wants to delete.
                                if (MessageBox.Show($"Are you sure you want to delete \"{SelectedScrapper.Settings.Telegram.KeyWord}\"?",
                                    "Confirm Deletion", MessageBoxButton.YesNo,
                                    MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                                    return;

                                SelectedScrapper.Settings.Telegrams.Remove(SelectedScrapper.Settings.Telegram);

                                SelectedScrapper.Settings.Telegram = new Models.Telegram();
                                break;

                            case "clear":
                                SelectedScrapper.Settings.Telegram = new Models.Telegram();
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand LinkCommand { get; set; }

        public void LinkExecute(object obj)
        {
            try
            {
                if (!DatabaseConnectionState)
                    return;

                if (!obj.ToString().Contains("http"))
                    return;

                var ads = DB();

                Advert.Clicks++;

                ads.Entry(Advert).State = EntityState.Modified;

                ads.SaveChanges();

                Process.Start(obj.ToString());
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        public ICommand UpdateActiveStateCommand { get; set; }

        public void UpdateActiveStateExecute(object obj)
        {
            try
            {
                if (!DatabaseConnectionState)
                    return;

                using (var con = DB())
                {
                    ActiveClient.IsActive = 0;

                    con.Entry(ActiveClient).State = EntityState.Modified;

                    con.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        #endregion

        #region Public Properties

        public string Version
        {
            get
            {
                var ver = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split('.');
                return $"Newegg Scrapper v{ver[0]}.{ver[1]}.{ver[2]}";
            }
        }

        private MainWindow _window;

        public MainWindow Window
        {
            get => _window;
            set { _window = value; OnPropertyChanged(); }
        }

        private object _displayContent;

        public object DisplayContent
        {
            get => _displayContent;
            set { _displayContent = value; OnPropertyChanged(); }
        }

        private ObservableCollection<ScrapperPreset> _scrappers = new ObservableCollection<ScrapperPreset>();

        public ObservableCollection<ScrapperPreset> Scrappers
        {
            get => _scrappers;
            set { _scrappers = value; OnPropertyChanged(); }
        }

        private ScrapperPreset _selectedScrapper = new ScrapperPreset();

        public ScrapperPreset SelectedScrapper
        {
            get => _selectedScrapper;
            set { _selectedScrapper = value; OnPropertyChanged(); }
        }

        private ObservableCollection<DebugReport> _debugReport = new ObservableCollection<DebugReport>();

        public ObservableCollection<DebugReport> DebugReport
        {
            get => _debugReport;
            set { _debugReport = value; OnPropertyChanged(); }
        }

        private int _currentlyRunning;

        public int CurrentlyRunning
        {
            get => _currentlyRunning;
            set { _currentlyRunning = value; OnPropertyChanged(); }
        }

        private bool _ipBanned;

        public bool IpBanned
        {
            get => _ipBanned;
            set
            {
                _ipBanned = value;

                OnPropertyChanged();

                if (value)
                {
                    StopSearches();
                }
            }
        }

        private DispatcherTimer _advertTimer;

        public DispatcherTimer AdvertTimer
        {
            get => _advertTimer;
            set { _advertTimer = value; OnPropertyChanged(); }
        }

        private ObservableCollection<HtmlLink> _adverts = new ObservableCollection<HtmlLink>();

        public ObservableCollection<HtmlLink> Adverts
        {
            get => _adverts;
            set { _adverts = value; OnPropertyChanged(); }
        }

        private HtmlLink _advert = new HtmlLink();

        public HtmlLink Advert
        {
            get => _advert;
            set { _advert = value; OnPropertyChanged(); }
        }

        private string _advertIds;

        public string AdvertIds
        {
            get => _advertIds ?? string.Empty;
            set { _advertIds = value; OnPropertyChanged(); }
        }

        private ActiveClients _activeClient = new ActiveClients();

        public ActiveClients ActiveClient
        {
            get => _activeClient;
            set { _activeClient = value; OnPropertyChanged(); }
        }

        #endregion

        #region Private Properties

        private readonly MediaPlayer _player = new MediaPlayer();

        private enum Duplicate
        {
            Discord,
            Telegram
        }

        private bool DatabaseConnectionState = false;

        private Random RandomNumber = new Random();

        #endregion

        #region Public Methods

        /// <summary>
        /// Saves execption into a list along with logging error into a file.
        /// </summary>
        /// <param name="e">Exception</param>
        /// <param name="filename"></param>
        public void WriteDebug(Exception e, string filename = "")
        {
            Message.Log(e, filename, MessageShow.No);

            //todo check if invoke here works
            //Application.Current.Dispatcher.Invoke(delegate // <--- HERE
            //{
            //    var debug = new DebugReport
            //    {
            //        ReportDate = $"{DateTime.Now:dd/MMM/yyyy HH:mm}",
            //        Exception = e
            //    };

            //    //add item to collection
            //    DebugReport.Add(debug);
            //});

            var debug = new DebugReport
            {
                ReportDate = $"{DateTime.Now:dd/MMM/yyyy HH:mm}",
                Exception = e
            };

            //add item to collection
            DebugReport.Add(debug);
        }

        #endregion

        #region Private Methods

        private void InitPresets()
        {
            if (!File.Exists(@"presets.cfg"))
                return;

            Scrappers = new ObservableCollection<ScrapperPreset>(Xml.Deserialize<ObservableCollection<ScrapperPreset>>(@"presets.cfg"));
        }

        private void SavePresets()
        {
            if (string.IsNullOrEmpty(SelectedScrapper.ID))
            {
                SelectedScrapper.ID = Guid.NewGuid().ToString();

                Scrappers.Add(SelectedScrapper);
            }

            Xml.Serialize<ObservableCollection<ScrapperPreset>>(@"presets.cfg", Scrappers);
        }

        private void StopSearches()
        {
            try
            {
                Scrappers.ForEach(fe =>
                {
                    fe.PresetState = false;

                    fe.Worker.CancelAsync();

                    fe.SearchFilters = new ObservableCollection<FilterBlock>();

                    fe.InStock = new ObservableCollection<ScrapperResult>();

                    fe.OutOfStock = new ObservableCollection<ScrapperResult>();
                });

                CurrentlyRunning = 0;
            }
            catch (Exception ex)
            {
                WriteDebug(ex);
            }
        }

        private void InitMediaPlayer()
        {
            //will change state of play/pause button, effectively changing the icon.
            _player.MediaEnded += delegate
            {
                SelectedScrapper.Settings.PlayerState = false;
            };
        }

        private bool IsDuplicateKeyword(Duplicate dup)
        {
            if (dup == Duplicate.Discord)
            {
                return SelectedScrapper.Settings.Discords.Any(a =>
                    a.KeyWord == SelectedScrapper.Settings.Discord.KeyWord
                    && a.Id != SelectedScrapper.Settings.Discord.Id);
            }
            
            return SelectedScrapper.Settings.Telegrams.Any(a =>
                a.KeyWord == SelectedScrapper.Settings.Telegram.KeyWord
                && a.Id != SelectedScrapper.Settings.Telegram.Id);
        }

        // ReSharper disable once UnusedMember.Local
        private async void DiscordApiTimeoutCheck()
        {
            //https://discord.com/api/webhooks/802929634810396784/X9hzclxLqVyUL_SqrbSEx768nMgrYQ_0vSm81L5K5a3x1Fw3R-0rbC_61N88QaF1CpgS

            // ReSharper disable once ConvertToConstant.Local
            var run = true;
            var timeout = 100;

            var webHook =
                $"https://discord.com/api/webhooks/802929634810396784/X9hzclxLqVyUL_SqrbSEx768nMgrYQ_0vSm81L5K5a3x1Fw3R-0rbC_61N88QaF1CpgS";

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            // ReSharper disable once LoopVariableIsNeverChangedInsideLoop
            while (run)
            {
                try
                {
                    Classes.Discord.SendDiscord(webHook, new ScrapperResult(){Desc = "Test to identify timeout"});

                    await Task.Delay(timeout);
                }
                catch
                {
                    // ReSharper disable once LocalizableElement
                    Console.WriteLine($"Discord Timeout {timeout} failed, increasing timeout by 100ms");
                    timeout += 100;
                }
            }
        }

        private Adverts DB()
        {
            //51.91.123.112
            //vps.vashbaldeus.pw

            var g =
                "u1SYB9y99JmlTlWTS5ophIoO6b12P4Dkle5xQXvNEb7UPvjHCGbSe9bXFP4SNNY3eps+mEWWnmBJzIs/58x/ns4l5d68lmifhMy0sfCPT8BYHdl2PeTpxo8kjpisa8bx";

            return new Adverts(
                $"data source=51.91.123.112;initial catalog=Scrapper;persist security info=True;user id=sa;password={g.Decrypt()};MultipleActiveResultSets=True;App=EntityFramework");
        }

        private void DatabaseConnectionTest()
        {
            var g =
                "u1SYB9y99JmlTlWTS5ophIoO6b12P4Dkle5xQXvNEb7UPvjHCGbSe9bXFP4SNNY3eps+mEWWnmBJzIs/58x/ns4l5d68lmifhMy0sfCPT8BYHdl2PeTpxo8kjpisa8bx";

            using (var conn = new SqlConnection($"Server=51.91.123.112;Database=Dealership2;User Id=sa;Password={g.Decrypt()};"))
            {
                try
                {
                    conn.Open();

                    conn.Close();

                    DatabaseConnectionState = true;
                }
                catch (Exception)
                {
                    DatabaseConnectionState = false;
                }
            }
        }

        private void InitAdvertTimer(TimeSpan time)
        {
            if (!DatabaseConnectionState)
                return;

            InitAdverts();

            AdvertTimer = new DispatcherTimer
            {
                Interval = time
            };

            AdvertTimer.Tick += delegate
            {
                InitAdverts();
            };

            AdvertTimer.Start();
        }

        private void InitAdverts()
        {
            try
            {
                var ads = DB();

                // load adverts if missing
                if (Adverts.Count == 0 || ads.HtmlLinks.Any(w => !AdvertIds.Contains(w.ID.ToString()) && w.HiddenAd == 0))
                {
                    Adverts = new ObservableCollection<HtmlLink>(ads.HtmlLinks.Where(w => w.HiddenAd == 0));

                    // save ids of adverts in string
                    // ReSharper disable once RedundantAssignment
                    AdvertIds = Adverts.Aggregate(string.Empty, (cur, i) => cur += $"{i.ID};");
                }
                //else if (ads.HtmlLinks.Any(w => !AdvertIds.Contains(w.ID.ToString()) && w.HiddenAd == 0))
                //{
                //    // pull new adverts from database
                //    var result = ads.HtmlLinks.Where(w => !AdvertIds.Contains(w.ID.ToString()) && w.HiddenAd == 0).ToList();

                //    // add new pulled ids to ids string
                //    // ReSharper disable once RedundantAssignment
                //    AdvertIds = result.Aggregate(AdvertIds, (cur, i) => cur += $"{i.ID};");

                //    // merge to collections into one & order by ID
                //    Adverts = new ObservableCollection<HtmlLink>(Adverts.Concat(result).OrderBy(ob => ob.ID));
                //}

                Advert = GetRandomAdvert();
            }
            catch (Exception ex)
            {
                Message.Log(ex, string.Empty, MessageShow.No);
            }
        }

        private HtmlLink GetRandomAdvert()
        {
            //var rnd = new Random();

            var index = RandomNumber.Next(0, Adverts.Count);

            //if randomly selected same advert, try again
            if (Advert.ID == Adverts[index].ID)
                GetRandomAdvert();

            return Adverts[index];
        }

        private void RegisterActiveClient()
        {
            try
            {
                if (!DatabaseConnectionState)
                    return;

                using (var con = DB())
                {
                    var cpu = ExtensionMethods.ExtensionMethods.CPUID();

                    //if exists pull data and update.
                    if (con.ActiveClients.Any(a => a.CPU_GUID == cpu))
                    {
                        ActiveClient = con.ActiveClients.FirstOrDefault(a => a.CPU_GUID == cpu);

                        if (ActiveClient == null) return;

                        ActiveClient.IsActive = 1;

                        con.Entry(ActiveClient).State = EntityState.Modified;

                        con.SaveChanges();
                    }
                    //if not registered, register new
                    else
                    {
                        ActiveClient = new ActiveClients
                        {
                            CPU_GUID = cpu,
                            IsActive = 1
                        };

                        con.Entry(ActiveClient).State = EntityState.Added;

                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        private DispatcherTimer AutoUpdateTimer = new DispatcherTimer();

        private bool AutoUpdateConnection = true;

        private void InitAutoUpdateTimer()
        {
            if (!AutoUpdateConnection)
                return;

            AutoUpdateTimer.Interval = new TimeSpan(1, 0, 0);

            AutoUpdateTimer.Tick += delegate
            {
                if (!AutoUpdateConnection)
                    return;

                CheckUpdate();
            };
        }

        private void CheckUpdate(bool defaultUpdate = false)
        {
            try
            {
                AutoUpdater.AppTitle = "Newegg Scrapper";

                AutoUpdater.Synchronous = true;

                AutoUpdater.Mandatory = true;

                AutoUpdater.UpdateMode = Mode.Forced;

                AutoUpdater.ShowSkipButton = false;

                AutoUpdater.ShowRemindLaterButton = false;

                AutoUpdater.DownloadPath = Environment.CurrentDirectory;

                AutoUpdater.ApplicationExitEvent += delegate
                {
                    Application.Current.Shutdown();
                };

                var currentDirectory = new DirectoryInfo(Environment.CurrentDirectory);

                if (currentDirectory.Parent != null)
                {
                    AutoUpdater.InstallationPath = currentDirectory.FullName;

                    // ReSharper disable once LocalizableElement
                    Debug.WriteLine($"{currentDirectory.FullName}");
                }

                AutoUpdater.ReportErrors = true;

                if (!defaultUpdate)
                {
                    AutoUpdater.CheckForUpdateEvent += delegate (UpdateInfoEventArgs args)
                    {
                        if (args == null) return;

                        if (!args.IsUpdateAvailable) return;

                        try
                        {
                            if (AutoUpdater.DownloadUpdate(args))
                            {
                                Window.Close();
                            }
                        }
                        catch (Exception exception)
                        {
                            Message.Log(exception);
                        }
                    };
                }

                const string ftp =
                    "Cw/FY8/R1FhHb95ax9xe1c/mJaf5uHtYQYrLIcdZn1sw0Ivf5vOTNymz9981RcE6sucPIE8hBoqk6IrFM6/yRR9Zb0RMjp9GrcrP386Ei/Q4vIFhG8eRklAR3gM5sh3U";

                AutoUpdater.Start(@"ftp://server191.web-hosting.com:21/public_html/subdomains/scrapper/version.xml", new NetworkCredential("vashlifp", ftp.Decrypt()));
            }
            catch (Exception ex)
            {
                Message.Log(ex, "", MessageShow.No);

                AutoUpdateConnection = false;
            }
        }

        #endregion
    }

    public class DebugReport : OnPropertyChange
    {
        private string _reportDate;

        public string ReportDate
        {
            get => _reportDate;
            set { _reportDate = value; OnPropertyChanged(); }
        }

        private Exception _exception;

        public Exception Exception
        {
            get => _exception;
            set { _exception = value; OnPropertyChanged(); }
        }
    }
}