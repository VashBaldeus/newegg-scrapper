using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace NeweggScrapper.Context
{
    public partial class Adverts : DbContext
    {
        public Adverts()
            : base("name=Adverts")
        {
        }

        public Adverts(string conn)
            : base(conn)
        {
        }

        public virtual DbSet<HtmlLink> HtmlLinks { get; set; }
        public virtual DbSet<ActiveClients> ActiveClients { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
