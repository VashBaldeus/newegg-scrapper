using ExtensionMethods;

namespace NeweggScrapper.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HtmlLink : OnPropertyChange
    {
        private int _id = 0;

        // ReSharper disable once InconsistentNaming
        public int ID
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        private string _html = string.Empty;

        public string Html
        {
            get => _html;
            set
            {
                _html = value;
                OnPropertyChanged();
            }
        }

        private string _imagesource = string.Empty;

        public string ImageSource
        {
            get => _imagesource;
            set
            {
                _imagesource = value;
                OnPropertyChanged();
            }
        }

        private double _clicks = 0;

        public double Clicks
        {
            get => _clicks;
            set
            {
                _clicks = value;
                OnPropertyChanged();
            }
        }

        private int _hiddenAd;

        public int HiddenAd
        {
            get => _hiddenAd;
            set { _hiddenAd = value; OnPropertyChanged(); }
        }
    }
}
