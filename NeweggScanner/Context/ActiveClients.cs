﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;
// ReSharper disable InconsistentNaming

namespace NeweggScrapper.Context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class ActiveClients : OnPropertyChange
    {
        private int _id;

        public int ID
        {
            get => _id;
            set { _id = value; OnPropertyChanged(); }
        }

        private string _cpu_GUID;

        public string CPU_GUID
        {
            get => _cpu_GUID;
            set { _cpu_GUID = value; OnPropertyChanged(); }
        }

        private int _isActive;

        public int IsActive
        {
            get => _isActive;
            set { _isActive = value; OnPropertyChanged(); }
        }
    }
}
