﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using ExtensionMethods;
using MahApps.Metro.Controls;
using Message = ExtensionMethods.Message;

namespace NeweggScrapper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DuplicateProcess.Check())
                {
                    Message.Show("An instance of the program already running, close it before starting a new one.",
                        "Duplicate Instance", MessageBoxButton.OK, MessageBoxImage.Information);

                    this.Close();
                }

                var dc = (MainViewModel)DataContext;

                if (dc == null)
                    return;

                dc.OnLoadCommand.Execute(null);

                dc.Window = this;

                foreach (var screen in Screen.AllScreens)
                {
                    var screenDetails = $"{screen.WorkingArea}".Split(',');

                    //var width = 
                    //    Regex.Replace(screenDetails[2].Split('=')[1], "[^0-9.]", "");
                    var height =
                        Regex.Replace(screenDetails[3].Split('=')[1], "[^0-9.]", "");

                    //Console.WriteLine($"{width}\n\n{height}");

                    if (Convert.ToInt32(height) <= 728)
                    {
                        this.ResizeMode = ResizeMode.CanResize;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Log(ex);
            }
        }

        private void SideDrawerHost_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            SideDrawerHost.IsLeftDrawerOpen = false;
        }

        private void BtnDrawerMenu_OnClick(object sender, RoutedEventArgs e)
        {
            SideDrawerHost.IsLeftDrawerOpen = true;
        }

        private void BtnDashboard_OnClick(object sender, RoutedEventArgs e)
        {
            SideDrawerHost.IsLeftDrawerOpen = false;
        }

        private void BtnBanMessage_OnClick(object sender, RoutedEventArgs e)
        {
            var dc = (MainViewModel)DataContext;

            if (dc == null)
                return;

            dc.IpBanned = false;

            dc.Scrappers.ForEach(fe => fe.IpBanned = false);
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            var dc = (MainViewModel)DataContext;

            if (dc == null)
                return;

            dc.UpdateActiveStateCommand.Execute(null);
        }
    }
}
