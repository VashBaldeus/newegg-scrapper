﻿using ExtensionMethods;

namespace NeweggScrapper.Models
{
    public class BlacklistWord : OnPropertyChange
    {
        private string _id;

        public string Id
        {
            get => _id;
            set { _id = value; OnPropertyChanged(); }
        }

        private string _word;

        public string Word
        {
            get => _word ?? string.Empty;
            set { _word = value; OnPropertyChanged(); }
        }
    }
}
