﻿using ExtensionMethods;
using HtmlAgilityPack;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;

// ReSharper disable InconsistentNaming
// ReSharper disable AccessToModifiedClosure

namespace NeweggScrapper.Models
{
    public class ScrapperPreset : OnPropertyChange
    {
        #region Properties

        private string _id;

        public string ID
        {
            get => _id ?? string.Empty;
            set { _id = value; OnPropertyChanged(); }
        }

        private ScrapperSettings _settings = new ScrapperSettings();

        public ScrapperSettings Settings
        {
            get => _settings;
            set { _settings = value; OnPropertyChanged(); }
        }

        private BackgroundWorker _worker = new BackgroundWorker
        {
            WorkerSupportsCancellation = true
        };
        [XmlIgnore]
        public BackgroundWorker Worker
        {
            get => _worker;
            set { _worker = value; OnPropertyChanged(); }
        }

        private bool _presetState;
        [XmlIgnore]
        public bool PresetState
        {
            get => _presetState;
            set { _presetState = value; OnPropertyChanged(); }
        }

        private string _name;

        public string Name
        {
            get => _name ?? string.Empty;
            set { _name = value; OnPropertyChanged(); }
        }

        private string _searchURL;

        public string SearchURL
        {
            get => _searchURL ?? string.Empty;
            set { _searchURL = value; OnPropertyChanged(); }
        }

        private bool _loop;

        public bool Loop
        {
            get => _loop;
            set { _loop = value; OnPropertyChanged(); }
        }

        private int _loopInterval;

        public int LoopInterval
        {
            get => _loopInterval;
            set { _loopInterval = value; OnPropertyChanged(); }
        }

        private string _category;
        [XmlIgnore]
        public string SearchCategory
        {
            get => _category ?? string.Empty;
            set { _category = value; OnPropertyChanged(); }
        }

        private string _regionSearch;
        [XmlIgnore]
        public string RegionSearch
        {
            get => _regionSearch;
            set { _regionSearch = value; OnPropertyChanged(); }
        }

        private ObservableCollection<FilterBlock> _searchFilters = new ObservableCollection<FilterBlock>();
        [XmlIgnore]
        public ObservableCollection<FilterBlock> SearchFilters
        {
            get => _searchFilters;
            set { _searchFilters = value; OnPropertyChanged(); }
        }

        private ObservableCollection<ScrapperResult> _inStock = new ObservableCollection<ScrapperResult>();
        [XmlIgnore]
        public ObservableCollection<ScrapperResult> InStock
        {
            get => _inStock;
            set { _inStock = value; OnPropertyChanged(); }
        }

        private ObservableCollection<ScrapperResult> _outOfStock = new ObservableCollection<ScrapperResult>();
        [XmlIgnore]
        public ObservableCollection<ScrapperResult> OutOfStock
        {
            get => _outOfStock;
            set { _outOfStock = value; OnPropertyChanged(); }
        }
        
        private ObservableCollection<ScrapperResult> _items = new ObservableCollection<ScrapperResult>();
        [XmlIgnore]
        public ObservableCollection<ScrapperResult> Items
        {
            get => _items;
            set { _items = value; OnPropertyChanged(); }
        }

        private ObservableCollection<ScrapperResult> _duplicates = new ObservableCollection<ScrapperResult>();
        [XmlIgnore]
        public ObservableCollection<ScrapperResult> Duplicates
        {
            get => _duplicates;
            set { _duplicates = value; OnPropertyChanged(); }
        }

        private string _searchResults;
        [XmlIgnore]
        public string SearchResults
        {
            get => _searchResults ?? string.Empty;
            set { _searchResults = value; OnPropertyChanged(); }
        }

        private bool _ipBanned;
        [XmlIgnore]
        public bool IpBanned
        {
            get => _ipBanned;
            set { _ipBanned = value; OnPropertyChanged(); }
        }

        private MainViewModel _m;
        [XmlIgnore]
        public MainViewModel M
        {
            get => _m;
            set { _m = value; OnPropertyChanged(); }
        }

        #endregion

        #region Constants

        [XmlIgnore]
        public const string SOLD_OUT = "sold out";

        [XmlIgnore]
        public const string VIEW_DETAILS = "view details";

        [XmlIgnore]
        public const string AUTO_NOTIFY = "auto notify";

        [XmlIgnore]
        public const string OUT_OF_STOCK = "out of stock";

        [XmlIgnore]
        public const string NOTIFY = "auto notify";

        [XmlIgnore]
        public const string SEARCHING_IN = "Searching in:";

        [XmlIgnore]
        private const string HTML_OPEN1 = "<!DOCTYPE html><html><head><style>body{font-family: arial, sans-serif;}" +
                                          "table {font-family: arial, sans-serif;border-collapse: collapse; width: 100%;}th{background-color:#f5c449;} td, th {border: 1px solid #dddddd; text-align: left;padding: 8px;}tr:nth-child(even){background-color: #dddddd;}</style></head><body><h2> ";
        [XmlIgnore]
        private const string HTML_OPEN2 = " RESULTS</h2>" +
                                          "<table><tr><th> BRAND </th><th> DESCRIPTION </th><th> PRICE </th><th> LINK </th><th> Add To Cart </th></tr> ";
        [XmlIgnore]
        private string HTML_BODY = string.Empty;

        [XmlIgnore]
        private const string HTML_CLOSE = "</table></body></html>";

        [XmlIgnore]
        private ObservableCollection<IntervalMessage> _intervalsDiscord = new ObservableCollection<IntervalMessage>();

        [XmlIgnore]
        public ObservableCollection<IntervalMessage> IntervalsDiscord
        {
            get => _intervalsDiscord;
            set { _intervalsDiscord = value; OnPropertyChanged(); }
        }

        [XmlIgnore]
        private ObservableCollection<IntervalMessage> _intervalsTelegram = new ObservableCollection<IntervalMessage>();

        [XmlIgnore]
        public ObservableCollection<IntervalMessage> IntervalsTelegram
        {
            get => _intervalsTelegram;
            set { _intervalsTelegram = value; OnPropertyChanged(); }
        }

        #endregion

        #region Private Properties

        [XmlIgnore]
        private bool classExist;

        [XmlIgnore]
        private static string _inStockCountForHtml = string.Empty;

        #endregion

        #region Methods

        #region Public Methods

        public async void InitScrapper(MainViewModel m)
        {
            try
            {
                if (M == null)
                    M = m;

                if (PresetState)
                {
                    //InStock = new ObservableCollection<ScrapperResult>();

                    OutOfStock = new ObservableCollection<ScrapperResult>();

                    m.DebugReport = new ObservableCollection<DebugReport>();
                }

                do
                {
                    if (!PresetState)
                        return;

                    //your ip was banned
                    if (IpBanned)
                    {
                        m.IpBanned = IpBanned;
                        PresetState = false;
                        return;
                    }

                    SearchFilters = new ObservableCollection<FilterBlock>();

                    ScrapeData(SearchURL);

                    if (InStock.Count > 0)
                    {
                        //play sound once stock found.
                        if (Settings.NotifySound)
                        {
                            var media = new MediaPlayer();

                            if (!string.IsNullOrEmpty(Settings.Sound) && File.Exists(Settings.Sound))
                            {
                                media.Open(new Uri(Settings.Sound));

                                media.Play();
                            }
                            else if (File.Exists($@"{Directory.GetCurrentDirectory()}\notifysound.mp3"))
                            {
                                media.Open(new Uri($@"{Directory.GetCurrentDirectory()}\notifysound.mp3"));

                                media.Play();
                            }
                            else m.WriteDebug(new Exception("Sound file is missing, hence no sound notification heard."));
                        }

                        string[] link = null;

                        var itemCount = 0;

                        var discordTimeoutCount = 1;

                        //if (Settings.NotifyDiscord)
                        //{
                        //    var inStock = new ObservableCollection<ScrapperResult>();

                        //    foreach (var key in Settings.Discords)
                        //    {
                        //        if (!string.IsNullOrEmpty(key.KeyWord))
                        //        {
                        //            inStock = new ObservableCollection<ScrapperResult>(InStock.Where(w =>
                        //                w.Desc.ToLower().Contains(key.KeyWord.ToLower())));
                        //        }
                        //        else inStock = InStock;

                        //        foreach (var s in inStock)
                        //        {
                        //            var interval = s.IntervalsDiscord.FirstOrDefault(fod => fod.Id == key.Id);

                        //            if (interval == null || string.IsNullOrEmpty(interval.Id))
                        //            {
                        //                interval = new IntervalMessage { Id = key.Id };

                        //                Application.Current.Dispatcher.Invoke(delegate // <--- HERE
                        //                {
                        //                    s.IntervalsDiscord.Add(interval);
                        //                });
                        //            }

                        //            if (interval.Interval > 0)
                        //                interval.Interval -= LoopInterval;

                        //            if (interval.Interval == 0 && key != null)
                        //            {
                        //                Classes.Discord.SendDiscord(key.WebHook, s, key.RoleIds);

                        //                interval.Interval = key.MessageInterval * LoopInterval;

                        //                discordTimeoutCount++;

                        //                if (discordTimeoutCount == 4)
                        //                {
                        //                    await Task.Delay(1000);

                        //                    discordTimeoutCount = 1;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                        foreach (var stock in InStock)
                        {
                            if (Settings.NotifyDiscord)
                            {
                                try
                                {
                                    if (Settings.Discords.Any(a => stock.Desc.ToLower().Contains(a.KeyWord.ToLower())
                                                                   || string.IsNullOrEmpty(a.KeyWord) && !string.IsNullOrEmpty(a.WebHook)))
                                    {
                                        var keywords =
                                            Settings.Discords.Where(a =>
                                                stock.Desc.ToLower().Contains(a.KeyWord.ToLower()) ||
                                                string.IsNullOrEmpty(a.KeyWord) && !string.IsNullOrEmpty(a.WebHook))
                                            .ToList();

                                        foreach (var keyword in keywords)
                                        {
                                            Debug.WriteLine($"STOCK INTERVALS COUNT [{IntervalsDiscord.Count}]");

                                            var interval =
                                                IntervalsDiscord.Any(fod =>
                                                    fod.Id == keyword.Id && fod.ItemDesc.Equals(stock.Desc))
                                                    ? IntervalsDiscord.First(fod =>
                                                        fod.Id == keyword.Id && fod.ItemDesc.Equals(stock.Desc))
                                                    : new IntervalMessage();

                                            Debug.WriteLine($"INTERVAL ID {interval.Id}");

                                            if (/*interval == null || */string.IsNullOrEmpty(interval.Id))
                                            {
                                                interval = new IntervalMessage {Id = keyword.Id, ItemDesc = stock.Desc};

                                                IntervalsDiscord.Add(interval);

                                                Debug.WriteLine($"STOCK INTERVALS COUNT [{IntervalsDiscord.Count}]");
                                            }

                                            if (interval.Interval > 0)
                                                interval.Interval -= LoopInterval;

                                            Debug.WriteLine($"Interval {interval.Interval}");

                                            if (interval.Interval == 0 && keyword != null)
                                            {
                                                Classes.Discord.SendDiscord(keyword.WebHook, stock, keyword.RoleIds);

                                                interval.Interval = keyword.MessageInterval * LoopInterval;

                                                discordTimeoutCount++;

                                                if (discordTimeoutCount == 4)
                                                {
                                                    await Task.Delay(1000);

                                                    discordTimeoutCount = 1;

                                                    Debug.WriteLine("Discord Timeout initiated.");
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    M.WriteDebug(ex, Name);
                                }
                            }

                            if (Settings.NotifyTelegram) 
                            {
                                try
                                {
                                    if (Settings.Telegrams.Any(a => stock.Desc.ToLower().Contains(a.KeyWord.ToLower())
                                                                    || string.IsNullOrEmpty(a.KeyWord) && !string.IsNullOrEmpty(a.Channel)))
                                    {
                                        var keywords =
                                            Settings.Telegrams.Where(a =>
                                                    stock.Desc.ToLower().Contains(a.KeyWord.ToLower()) ||
                                                    string.IsNullOrEmpty(a.KeyWord) && !string.IsNullOrEmpty(a.Channel))
                                                .ToList();

                                        foreach (var keyword in keywords)
                                        {
                                            //var interval =
                                            //    IntervalsTelegram.FirstOrDefault(fod => fod.Id == keyword.Id && fod.ItemDesc.Equals(stock.Desc));

                                            var interval =
                                                IntervalsTelegram.Any(fod =>
                                                    fod.Id == keyword.Id && fod.ItemDesc.Equals(stock.Desc))
                                                    ? IntervalsTelegram.First(fod =>
                                                        fod.Id == keyword.Id && fod.ItemDesc.Equals(stock.Desc))
                                                    : new IntervalMessage();

                                            if (/*interval == null || */string.IsNullOrEmpty(interval.Id))
                                            {
                                                interval = new IntervalMessage { Id = keyword.Id, ItemDesc = stock.Desc };

                                                IntervalsTelegram.Add(interval);
                                            }

                                            try
                                            {
                                                link = stock.Link.Split('?');
                                            }
                                            catch (Exception exception)
                                            {
                                                m.WriteDebug(exception);
                                            }

                                            if (interval.Interval > 0)
                                                interval.Interval -= LoopInterval;

                                            if (interval.Interval == 0 || !keyword.PrvMessage.Contains(stock.Link))
                                            {
												var message = TelegramEscapeChars(stock.Desc);
                                                var msgPrice = TelegramEscapeChars(stock.Price);

                                                var msg = new[]
                                                {
                                                    $"{stock.Brand}\n\n",
                                                    //$"{stock.Desc}\n\n",
                                                    $"Product Page\n",
                                                    $"[{message}]({stock.Link})\n\n",
                                                    $"{msgPrice}\n\n",
                                                    $"Add To Cart\n",
                                                    $"[CLICK]({stock.AddToCart})\n\n",
                                                    "\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\n"
                                                };
                                               
                                                // ReSharper disable once RedundantAssignment
                                                keyword.Message += msg.Aggregate(string.Empty, (cur, i) => cur += $"{i}");

                                                keyword.PrvMessage += $"{stock.Link}|||";

                                                interval.Interval = keyword.MessageInterval * LoopInterval;

                                                keyword.MessageCounter++;

                                                await Task.Delay(500);
                                            }

                                            itemCount++;

                                            //if 5 items were grouped or end of collection reached, send message.
                                            if ((keyword.MessageCounter == 5 || itemCount == InStock.Count) && !string.IsNullOrEmpty(keyword.Message))
                                            {
                                                Classes.Telegram.SendMessage(keyword.Key, keyword.Channel,  keyword.Message);

                                                //Console.WriteLine($"Telegram Message: {stock.Desc.Substring(0, 15)}");

                                                keyword.Message = string.Empty;

                                                keyword.MessageCounter = 0;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    M.WriteDebug(ex, Name);
                                }
                            }
                        }

                        //will send any unsent messages to telegram
                        if (Settings.Telegrams.Any(a => !string.IsNullOrEmpty(a.Message) && a.MessageCounter > 0 && a.MessageCounter <= 5))
                        {
                            Settings.Telegrams.Where(a =>
                                    !string.IsNullOrEmpty(a.Message) && a.MessageCounter > 0 && a.MessageCounter <= 5)
                                .ForEach(keyword =>
                                {
                                    Classes.Telegram.SendMessage(keyword.Key, keyword.Channel, keyword.Message);

                                    keyword.Message = string.Empty;

                                    keyword.MessageCounter = 0;
                                });
                        }

                        //TODO rework mailing system, as current method will cause spamming.
                        //if (Settings.NotifyEmail)
                        //{
                        //    try
                        //    {
                        //        var email = new Email(m);

                        //        foreach (var e in Settings.Emails)
                        //        {
                        //            email.SendEmail(e, SearchResults);
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        M.WriteDebug(ex, Name);
                        //    }
                        //}
                    }

                    if (Loop)
                        await Task.Delay((LoopInterval > 0) ? LoopInterval * 1000 : 0);

                } while (Loop && PresetState);

                PresetState = false;
            }
            catch (Exception ex)
            {
                M.WriteDebug(ex, Name);
            }
        }

        public void DiscordAlert()
        {
            try
            {

            }
            catch (Exception ex)
            {
                M.WriteDebug(ex, Name);
            }
        }

        public void TelegramAlert()
        {
            try
            {

            }
            catch (Exception ex)
            {
                M.WriteDebug(ex, Name);
            }
        }

        #endregion

        #region Private Methods

        private void ScrapeData(string page)
        {
            SearchResults = string.Empty;
            HTML_BODY = string.Empty;

            var doc = new HtmlDocument();

            var web = InitHtmlWeb(page, out var resultsHtml, out doc);

            Debug.WriteLine($"Doc {doc.ParsedText.Length}");

            // number of pages in search result
            ChechHowManyPages(doc, out var pages);
            // ReSharper disable once RedundantAssignment
            resultsHtml = GetItemResults(page, pages, web, resultsHtml);

            SearchResults += HTML_OPEN1 + _inStockCountForHtml + HTML_OPEN2;
            SearchResults += HTML_BODY;
            SearchResults += HTML_CLOSE;
        }

        private HtmlWeb InitHtmlWeb(string page, out string resultHtml, out HtmlDocument doc)
        {
            var web = new HtmlWeb();

            var uri = new Uri(page);

            resultHtml = string.Empty;

            web.PreRequest = request =>
            {
                try
                {
                    var cookieContainer = new CookieContainer();

                    // set cookies according location
                    SetCookieByLocation(page, cookieContainer, uri);
                    request.CookieContainer = cookieContainer;

                    return true;
                }
                catch (Exception ex)
                {
                    M.WriteDebug(ex, Name);

//#if DEBUG
//                    /*
//                     * an attempt to mitigate the issue where connection to NewEgg is working but pulling HTML data times out or no connection to server,
//                     * very odd issue, almost impossible to receate in debug mode.
//                     */
//                    if (ex.Message.Contains("The operation has timed out") || ex.Message.Contains("Unable to connect to the remote server"))
//                    {
//                        InitHtmlWeb(page, out var resultsHtml, doc);
//                    }
//#endif
                }

                return true;
            };

            doc = web.Load(page);

            return web;
        }

        private void SetCookieByLocation(string page, CookieContainer cookieContainer, Uri uri)
        {
            switch (page)
            {
                case var x when x.Contains("global/il-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "ISR"));
                    RegionSearch = $"{SEARCHING_IN} Israeli Store";
                    break;
                case var x when x.Contains("global/uk-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "GBR"));
                    RegionSearch = $"{SEARCHING_IN} UK Store";
                    break;
                case var x when x.Contains("global/tr-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "TUR"));
                    RegionSearch = $"{SEARCHING_IN} Turkish Store";
                    break;
                case var x when x.Contains("www.newegg.ca"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "CAN"));
                    RegionSearch = $"{SEARCHING_IN} Canada Store";
                    break;
                case var x when x.Contains("global/mx-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "MEX"));
                    RegionSearch = $"{SEARCHING_IN} Mexico Store";
                    break;
                case var x when x.Contains("global/au-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "AUS"));
                    RegionSearch = $"{SEARCHING_IN} AUS Store";
                    break;
                case var x when x.Contains("global/nz-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "NZL"));
                    RegionSearch = $"{SEARCHING_IN} New-Z Store";
                    break;
                case var x when x.Contains("global/ar-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "ARG"));
                    RegionSearch = $"{SEARCHING_IN} Argentina Store";
                    break;
                case var x when x.Contains("global/bh-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "BHR"));
                    RegionSearch = $"{SEARCHING_IN} Bahrain Store";
                    break;
                case var x when x.Contains("global/kw-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "KWT"));
                    RegionSearch = $"{SEARCHING_IN} Kuwait Store";
                    break;
                case var x when x.Contains("global/om-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "OMN"));
                    RegionSearch = $"{SEARCHING_IN} Oman Store";
                    break;
                case var x when x.Contains("global/qa-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "QAT"));
                    RegionSearch = $"{SEARCHING_IN} Qatar Store";
                    break;
                case var x when x.Contains("global/sa-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "SAU"));
                    RegionSearch = $"{SEARCHING_IN} Saudi-Arab Store";
                    break;
                case var x when x.Contains("global/ae-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "ARE"));
                    RegionSearch = $"{SEARCHING_IN} UAE Store";
                    break;
                case var x when x.Contains("global/hk-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "HKG"));
                    RegionSearch = $"{SEARCHING_IN} Hong-Kong Store";
                    break;
                case var x when x.Contains("global/jp-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "JPN"));
                    RegionSearch = $"{SEARCHING_IN} Japan Store";
                    break;
                case var x when x.Contains("global/ph-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "PHL"));
                    RegionSearch = $"{SEARCHING_IN} Philippines Store";
                    break;
                case var x when x.Contains("global/sg-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "SGP"));
                    RegionSearch = $"{SEARCHING_IN} Singapore Store";
                    break;
                case var x when x.Contains("global/kr-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "KOR"));
                    RegionSearch = $"{SEARCHING_IN} Korea Store";
                    break;
                case var x when x.Contains("global/th-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "THA"));
                    RegionSearch = $"{SEARCHING_IN} Thai Store";
                    break;
                case var x when x.Contains("global/in-en"):
                    cookieContainer.Add(uri, new Cookie("NV%5FW57", "IND"));
                    RegionSearch = $"{SEARCHING_IN} Indian Store";
                    break;
                default:
                    RegionSearch = $"{SEARCHING_IN} USA Store";
                    break;
            }
        }

        private void ChechHowManyPages(HtmlDocument doc, out string pages)
        {
            pages = string.Empty;

            classExist = IsClassExist(doc, "span", "class", "list-tool-pagination-text");

            if (classExist)
            {
                pages = doc.DocumentNode.SelectSingleNode("//*[@class = 'list-tool-pagination-text']").InnerText;

                Debug.WriteLine($"ChechHowManyPages [{pages}]");
            }

            //true if our IP was banned, false if not.
            IpBanned = IsClassExist(doc, "div", "class", "page-content page-404");
        }

        private static bool IsClassExist(HtmlNode item, string htmlTag, string attribute, string attributeValue)
        {
            try
            {
                var isclassExist = item.ChildNodes
                    .Descendants(htmlTag)
                    .Any(d => d.GetAttributeValue(attribute, "") == attributeValue);

                return isclassExist;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool IsClassExist(HtmlDocument doc, string htmlTag, string attribute, string attributeValue)
        {
            try
            {
                var isclassExist = doc.DocumentNode
                    .Descendants(htmlTag)
                    .Any(d => d.GetAttributeValue(attribute, "") == attributeValue);

                return isclassExist;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string GetItemResults(string page, string pages, HtmlWeb web, string resultsHtml)
        {
            HtmlDocument doc = null;
            var numOfPages = 1;

            if (!string.IsNullOrEmpty(pages))
            {
                numOfPages = Convert.ToInt32((pages.Contains("1/1")) ? "1" : pages.Split('/').GetValue(1).ToString());
            }

            // ReSharper disable once LocalizableElement
            Console.WriteLine($"Found Pages: {numOfPages}");

            // iterates number of pages returned in search
            for (var i = 1; i <= numOfPages; i++)
            {
                if (!page.Contains("&page="))
                    page += $"&page={i}";
                else
                {
                    page = page.Remove(page.Length - 1);
                    page += i;
                }

                doc = web.Load(page);

                web.UseCookies = true;

                var items = doc.DocumentNode.SelectNodes("//*[@class = 'item-cell']");

                // iterates all items in the current page
                resultsHtml = GetResults(items, resultsHtml, i);
            }

            //check if new found items exist in either inStock or OutofStock, remove if not found in either collection
            Application.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                for (var x = 0; x < InStock.Count; x++)
                {
                    if (Items.Any(a => a.Link == InStock[x].Link && a.Desc == InStock[x].Desc && a.StockState))
                        continue;

                    if (!InStock[x].StockState && IntervalsTelegram.Count > 0 && IntervalsDiscord.Count > 0)
                    {
                        if (IntervalsTelegram.Any(a => a.ItemDesc == InStock[x].Desc))
                        {
                            var inter = IntervalsTelegram.FirstOrDefault(a => a.ItemDesc == InStock[x].Desc);

                            if (inter != null)
                                IntervalsTelegram.Remove(inter);
                        }

                        if (IntervalsDiscord.Any(a => a.ItemDesc == InStock[x].Desc))
                        {
                            var inter = IntervalsDiscord.FirstOrDefault(a => a.ItemDesc == InStock[x].Desc);

                            if (inter != null)
                                IntervalsDiscord.Remove(inter);
                        }
                    }

                    InStock.Remove(InStock[x]);

                    x--;
                }

                for (var x = 0; x < OutOfStock.Count; x++)
                {
                    if (Items.Any(a => a.Link == OutOfStock[x].Link && a.Desc == OutOfStock[x].Desc && !a.StockState))
                        continue;

                    if (!OutOfStock[x].StockState && IntervalsTelegram.Count > 0 && IntervalsDiscord.Count > 0)
                    {
                        if (IntervalsTelegram.Any(a => a.ItemDesc == OutOfStock[x].Desc))
                        {
                            var inter = IntervalsTelegram.FirstOrDefault(a => a.ItemDesc == OutOfStock[x].Desc);

                            if (inter != null)
                                IntervalsTelegram.Remove(inter);
                        }

                        if (IntervalsDiscord.Any(a => a.ItemDesc == OutOfStock[x].Desc))
                        {
                            var inter = IntervalsDiscord.FirstOrDefault(a => a.ItemDesc == OutOfStock[x].Desc);

                            if (inter != null)
                                IntervalsDiscord.Remove(inter);
                        }
                    }

                    OutOfStock.Remove(OutOfStock[x]);

                    x--;
                }
            });

            AddSearchFiltersDynamically(doc);

            return resultsHtml;
        }

        private string GetResults(HtmlNodeCollection items, string resultsHtml, int pages)
        {
            try
            {
                //no results
                if (items == null)
                    return string.Empty;

                //first page? reset collection.
                if (pages == 1)
                {
                    //reset all found items list
                    Items = new ObservableCollection<ScrapperResult>();

                    Duplicates = new ObservableCollection<ScrapperResult>();
                }

                // ReSharper disable once LocalizableElement
                Console.WriteLine($"Page {pages}");

                foreach (var item in items)
                {
                    var title = string.Empty;

                    //assign item title
                    try
                    {
                        if (IsClassExist(item, "a", "class", "item-brand"))
                        {
                            title = HttpUtility.HtmlDecode(item.SelectSingleNode(".//a[@class = 'item-brand']").FirstChild
                                .Attributes["title"].Value);
                        }

                        if (IsClassExist(item, "a", "class", "item-img combo-img-0"))
                        {
                            title = "COMBO DEAL";
                        }

                        if (IsClassExist(item, "div", "class", "txt-ads-title"))
                            continue;
                    }
                    catch (Exception)
                    {
                        title = "NO BRAND ASSIGNED";
                    }

                    //current item price
                    var price = item.SelectSingleNode(".//div[@class = 'item-action']").FirstChild.InnerText;

                    //corrects item price
                    price = FixPriceString(ref resultsHtml, price, item);

                    //TODO feels a tad redundent check, need to double check if this still needed
                    price = CheckIfItemIsInStock(item, price);

                    //correct price string for missing ending
                    if (price.Contains("Shippi"))
                        price += "ng";

                    //item name
                    var desc = HttpUtility.HtmlDecode(item.SelectSingleNode(".//a[@class = 'item-title']").InnerText);

                    //store link
                    var link = HttpUtility.HtmlDecode(title.Contains("COMBO DEAL")
                        ? item.SelectSingleNode(".//a[@class = 'item-img combo-img-0']").Attributes["href"].Value
                        : item.SelectSingleNode(".//a[@class = 'item-img']").Attributes["href"].Value);

                    //current item stock condition
                    var stockCondition =
                        HttpUtility.HtmlDecode(item.SelectSingleNode(".//div[@class = 'item-button-area']").FirstChild.InnerText).ToLower();

                    var itemPromo = string.Empty;

                    if (IsClassExist(item, "p", "class", "item-promo"))
                    {
                        try
                        {
                            itemPromo = HttpUtility.HtmlDecode(item.SelectSingleNode(".//p[@class = 'item-promo']").InnerText);
                        }
                        catch (Exception)
                        {
                            //
                        }
                    }

                    //current item in stock, when true in stock, false not in stock
                    var stockState =
                        stockCondition == SOLD_OUT
                          || stockCondition == VIEW_DETAILS
                          || itemPromo == OUT_OF_STOCK
                          || !price.Contains(OUT_OF_STOCK);

                    string rakutenMid = link.Contains("newegg.com") ? "44583" : "44589";
                    link = $"https://click.linksynergy.com/deeplink?id=MHFsI72E**o&mid={rakutenMid}&murl={link}";
                    var i = new ScrapperResult
                    {
                        Brand = title,
                        Desc = desc,
                        Link = link,
                        Price = price,
                        StockState = stockState
                    };

                    //add to cart list
                    i.AddToCart = AddToCartLink(i.Link, item);
                    var cartEncoded = Uri.EscapeDataString(i.AddToCart);
                    
                    i.AddToCart = "https://click.linksynergy.com/deeplink?id=MHFsI72E**o&mid={rakutenMid}&murl=" + cartEncoded;
                    Application.Current.Dispatcher.Invoke(delegate // <--- HERE
                    {
                        //to all items found list
                        Items.Add(i);
                    });

                    //add to in stock, if not in stock already
                    if (stockState
                        && InStock.Any(a => a.Desc.Equals(i.Desc)
                        && a.Link.Equals(i.Link)) == false)
                    {
                        var blklstDictionary = false;

                        if (Settings.BlacklistDictionary.Count > 0)
                        {
                            Settings.BlacklistDictionary.ForEach(fe =>
                            {
                                if (i.Desc.Contains(fe.Word))
                                {
                                    //description of item contains blacklisted word, do not add item to in-stock list.
                                    blklstDictionary = true;
                                }
                            });
                        }

                        if (!blklstDictionary && !itemPromo.ToLower().Contains("this item can only be purchased with a combo"))
                        {
                            HTML_BODY +=
                                $"<tr><td>{i.Brand}</td><td>{i.Desc}</td><td>{price}</td><td><a href=\"{i.Link}\">Go To Product</a></td><td><a href=\"{i.AddToCart}\">Add To Cart</a></td></tr>";
                            _inStockCountForHtml = InStock.Count.ToString();

                            Application.Current.Dispatcher.Invoke(delegate // <--- HERE
                            {
                                //add item to collection
                                InStock.Add(i);
                            });
                        }
                    }
                    //add to out of stock, if not in out of stock already
                    else if (OutOfStock.Any(a => a.Desc.Equals(i.Desc) && a.Link.Equals(i.Link)) == false)
                    {
                        Application.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            OutOfStock.Add(i);
                        });
                    }
                    //duplicate
                    //else
                    //{
                    //    Application.Current.Dispatcher.Invoke(delegate // <--- HERE
                    //    {
                    //        Duplicates.Add(i);
                    //    });
                    //}
                }

                //Console.WriteLine($"Duplicates found: {Duplicates.Count}");

                return string.IsNullOrEmpty(resultsHtml) ? string.Empty : resultsHtml;
            }
            catch (Exception ex)
            {
                M.WriteDebug(ex, Name);
                return string.Empty;
            }
        }

        private static string FixPriceString(ref string resultsHtml, string price, HtmlNode item)
        {
            //remove unwanted chars from price string
            if (string.IsNullOrEmpty(price)) return price;

            price = price.Remove(price.Length - 2, 2);

            price = price.Remove(0, 1);

            resultsHtml += item.OuterHtml;

            return price;
        }

        private string CheckIfItemIsInStock(HtmlNode item, string price)
        {
            var itemPromo = string.Empty;

            if (IsClassExist(item, "p", "class", "item-promo"))
            {
                try
                {
                    itemPromo = HttpUtility.HtmlDecode(item.SelectSingleNode(".//p[@class = 'item-promo']").InnerText);
                }
                catch (Exception ex)
                {
                    M.WriteDebug(ex, Name);
                }
            }

            var itemButton =
                HttpUtility.HtmlDecode(item.SelectSingleNode(".//div[@class = 'item-button-area']").FirstChild.InnerText).Trim(' ');

            if (itemButton.ToLower().Contains(SOLD_OUT)
                || itemPromo.ToLower().Contains(OUT_OF_STOCK)
                //|| itemButton.ToLower().Contains(VIEW_DETAILS) TODO disabled for now to test
                //|| itemPromo.ToLower().Contains(VIEW_DETAILS)
                || itemButton.ToLower().Contains(NOTIFY))
            {
                //price = itemButton.ToLower().Contains(NOTIFY) ? itemPromo : $"{itemPromo}\n{itemButton}";
                price = OUT_OF_STOCK;
            }

            return price;
        }

        private string AddToCartLink(string itemLink, HtmlNode item)
        {
            // ReSharper disable once TooWideLocalVariableScope
            string[] link;
            var cartLink = string.Empty;
            var itemNumber = string.Empty;
            var setLocation = string.Empty;

            try
            {
                classExist = IsClassExist(item, "div", "class", "item-stock");
                if (classExist)
                {
                    itemNumber = HttpUtility.HtmlDecode(item.SelectSingleNode(".//div[@class = 'item-stock']").Attributes["id"].Value);
                    var fixNumber = itemNumber.Split('_');
                    if (fixNumber[1].Contains("-"))
                    {
                        var result = string.Concat(fixNumber[1].Where(c => !char.IsPunctuation(c)));
                        itemNumber = result;
                    }
                    else
                    {
                        itemNumber = fixNumber[1];
                    }

                }

                if (!itemLink.ToLower().Contains("combodeal"))
                {
                    link = itemLink.Split('?');
                    var cart = link[1].Split('/');
                    foreach (var t in cart)
                    {
                        if (t == "p")
                        {
                            cartLink = !itemNumber.Contains("9SIA") ? $"N82E168{itemNumber}" : itemNumber;
                        }
                    }
                }
                else
                {
                    // ReSharper disable once RedundantAssignment
                    link = itemLink.Split('?');

                    var cart = itemLink.Split('=');

                    cartLink = cart[4];
                }

            }
            catch (Exception)
            {
                //
            }

            var location = itemLink.Split('/');

            for (var i = 0; i < location.Length; i++)
            {
                if (location[i].Contains("global"))
                {
                    setLocation = $"global/{location[i + 1]}/";
                }
            }

            string dotCom = itemLink.Contains("newegg.com") ? "com" : "ca";

            var addToCart = 
                $"https://secure.newegg.{dotCom}/{setLocation}Shopping/AddtoCart.aspx?Submit=ADD&ItemList={cartLink}";

            return addToCart;
        }

        private void AddSearchFiltersDynamically(HtmlDocument doc)
        {
            try
            {
                HtmlNodeCollection filters = null;

                if (IsClassExist(doc, "ul", "class", "filter-choices"))
                {
                    filters = doc.DocumentNode.SelectNodes("//*[@class = 'filter-choices']");
                }

                if (IsClassExist(doc, "li", "class", "is-current"))
                {
                    SearchCategory = doc.DocumentNode.SelectSingleNode("//*[@class = 'is-current']").InnerText;
                }

                if (filters != null)
                {
                    foreach (var filter in filters)
                    {
                        var list = filter.SelectNodes(".//li");
                        foreach (var li in list)
                        {
                            var header = string.Empty;

                            var filterStr = string.Empty;

                            try
                            {
                                if (li.SelectSingleNode(".//label[@class = 'filter-choice-title']") == null)
                                    throw new Exception(string.Empty);

                                header = HttpUtility.HtmlDecode(li.SelectSingleNode(".//label[@class = 'filter-choice-title']").InnerText);

                                if (li.SelectNodes(".//span[@class = 'filter-choice']") == null)
                                    throw new Exception(string.Empty);

                                var content = (li.SelectNodes(".//span[@class = 'filter-choice']"));

                                filterStr = content.Aggregate(filterStr, (current, cont) => current + $"{HttpUtility.HtmlDecode(cont.InnerText)},");
                            }
                            catch (Exception ex)
                            {
                                if(!string.IsNullOrEmpty(ex.Message))
                                    M.WriteDebug(ex, Name);
                            }

                            if (!SearchFilters.Any(a =>
                                a.Block == header || a.Block == string.Empty))
                            {
                                var f = new FilterBlock
                                {
                                    Block = header,
                                };

                                var filterSplit = filterStr.Split(',');

                                foreach (var s in filterSplit)
                                {
                                    if (string.IsNullOrEmpty(s))
                                        continue;

                                    f.Panels.Add(s);
                                }

                                Application.Current.Dispatcher.Invoke(delegate
                                {
                                    SearchFilters.Add(f);

                                    //remove filters without header
                                    for (var i = 0; i < SearchFilters.Count; i++)
                                    {
                                        if (SearchFilters[i].Block != string.Empty)
                                            continue;

                                        SearchFilters.RemoveAt(i);
                                        i--;
                                    }
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                M.WriteDebug(ex, Name);
            }
        }

        private string TelegramEscapeChars(string text)
        {
            var chars = new[]
            {
                "}",
                "!",
                "|",
                "{",
                "`",
                "#",
                "=",
                "]",
                "~",
                "<",
                "_",
                "*",
                "[",
                ">",
                "+",
                "-",
                ".",
                "(",
                ")",
            };

            var result = string.Empty;

            foreach (var c in chars)
            {
                if(!text.Contains(c))
                    continue;

                result = text.Replace(c, $"\\{c}");
            }

            return string.IsNullOrEmpty(result) ? text : result;
        }

        #endregion

        #endregion
    }

    public class ScrapperResult : OnPropertyChange
    {
        private string _brand;

        public string Brand
        {
            get => _brand;
            set { _brand = value; OnPropertyChanged(); }
        }

        private string _desc;

        public string Desc
        {
            get => _desc;
            set { _desc = value; OnPropertyChanged(); }
        }

        private string _link;

        public string Link
        {
            get => _link;
            set { _link = value; OnPropertyChanged(); }
        }

        private string _addToCart;

        public string AddToCart
        {
            get => _addToCart;
            set { _addToCart = value; OnPropertyChanged(); }
        }

        private string _price;

        public string Price
        {
            get => _price;
            set { _price = value; OnPropertyChanged(); }
        }

        private int _discordInterval;

        public int DiscordInterval
        {
            get => _discordInterval;
            set { _discordInterval = value; OnPropertyChanged(); }
        }

        private int _messageTelegramInterval;

        public int TelegramInterval
        {
            get => _messageTelegramInterval;
            set { _messageTelegramInterval = value; OnPropertyChanged(); }
        }

        private bool _stockState;

        public bool StockState
        {
            get => _stockState;
            set { _stockState = value; OnPropertyChanged(); }
        }

        //private ObservableCollection<IntervalMessage> _intervalsDiscord = new ObservableCollection<IntervalMessage>();

        //public ObservableCollection<IntervalMessage> IntervalsDiscord
        //{
        //    get => _intervalsDiscord;
        //    set { _intervalsDiscord = value; OnPropertyChanged(); }
        //}

        //private ObservableCollection<IntervalMessage> _intervalsTelegram = new ObservableCollection<IntervalMessage>();

        //public ObservableCollection<IntervalMessage> IntervalsTelegram
        //{
        //    get => _intervalsTelegram;
        //    set { _intervalsTelegram = value; OnPropertyChanged(); }
        //}
    }

    public class FilterBlock : OnPropertyChange
    {
        private string _block;

        public string Block
        {
            get => _block ?? string.Empty;
            set { _block = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> _panels = new ObservableCollection<string>();

        public ObservableCollection<string> Panels
        {
            get => _panels;
            set { _panels = value; OnPropertyChanged(); }
        }
    }
}