﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;
using ExtensionMethods;

namespace NeweggScrapper.Models
{
    public class ScrapperSettings : OnPropertyChange
    {
        private bool _notifyEmail;

        public bool NotifyEmail
        {
            get => _notifyEmail;
            set { _notifyEmail = value; OnPropertyChanged(); }
        }

        private bool _notifySound;

        public bool NotifySound
        {
            get => _notifySound;
            set { _notifySound = value; OnPropertyChanged(); }
        }

        private bool _notifyTelegram;

        public bool NotifyTelegram
        {
            get => _notifyTelegram;
            set { _notifyTelegram = value; OnPropertyChanged(); }
        }

        private bool _notifyDiscord;

        public bool NotifyDiscord
        {
            get => _notifyDiscord;
            set { _notifyDiscord = value; OnPropertyChanged(); }
        }

        private string _sound;

        public string Sound
        {
            get => _sound ?? string.Empty;
            set { _sound = value; OnPropertyChanged(); }
        }

        private string _mail;
        [XmlIgnore]
        public string Mail
        {
            get => _mail ?? string.Empty;
            set { _mail = value; OnPropertyChanged(); }
        }

        private ObservableCollection<Telegram> _telegrams = new ObservableCollection<Telegram>();

        public ObservableCollection<Telegram> Telegrams
        {
            get => _telegrams;
            set { _telegrams = value; OnPropertyChanged(); }
        }

        private Telegram _selectedTelegram = new Telegram();
        [XmlIgnore]
        public Telegram Telegram
        {
            get => _selectedTelegram;
            set { _selectedTelegram = value; OnPropertyChanged(); }
        }

        private ObservableCollection<Discord> _discords = new ObservableCollection<Discord>();

        public ObservableCollection<Discord> Discords
        {
            get => _discords;
            set { _discords = value; OnPropertyChanged(); }
        }

        private Discord _selectedDiscord = new Discord();
        [XmlIgnore]
        public Discord Discord
        {
            get => _selectedDiscord;
            set { _selectedDiscord = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> _emails = new ObservableCollection<string>();

        public ObservableCollection<string> Emails
        {
            get => _emails;
            set { _emails = value; OnPropertyChanged(); }
        }

        private ObservableCollection<BlacklistWord> _blacklistDictionary = new ObservableCollection<BlacklistWord>();

        public ObservableCollection<BlacklistWord> BlacklistDictionary
        {
            get => _blacklistDictionary;
            set { _blacklistDictionary = value; OnPropertyChanged(); }
        }

        private BlacklistWord _selectedBlacklist = new BlacklistWord();
        [XmlIgnore]
        public BlacklistWord SelectedBlacklist
        {
            get => _selectedBlacklist;
            set { _selectedBlacklist = value; OnPropertyChanged(); }
        }

        private bool _playerState;
        [XmlIgnore]
        public bool PlayerState
        {
            get => _playerState;
            set { _playerState = value; OnPropertyChanged(); }
        }
    }

    public class Telegram : OnPropertyChange
    {
        private string _id;

        public string Id
        {
            get => _id ?? string.Empty;
            set { _id = value; OnPropertyChanged(); }
        }

        private string _name;

        public string Name
        {
            get => _name ?? string.Empty;
            set { _name = value; OnPropertyChanged(); }
        }

        private string _keyWord;

        public string KeyWord
        {
            get => _keyWord ?? string.Empty;
            set { _keyWord = value; OnPropertyChanged(); }
        }

        private string _key;

        public string Key
        {
            get => _key ?? string.Empty;
            set { _key = value; OnPropertyChanged(); }
        }

        private string _channel;

        public string Channel
        {
            get => _channel ?? string.Empty;
            set { _channel = value; OnPropertyChanged(); }
        }

        private int _messageInterval;

        public int MessageInterval
        {
            get => _messageInterval;
            set { _messageInterval = value; OnPropertyChanged(); }
        }

        private int _messageCounter;
        [XmlIgnore]
        public int MessageCounter
        {
            get => _messageCounter;
            set { _messageCounter = value; OnPropertyChanged(); }
        }

        private string _message;
        [XmlIgnore]
        public string Message
        {
            get => _message ?? string.Empty;
            set { _message = value; OnPropertyChanged(); }
        }

        private string _prvMessage;
        [XmlIgnore]
        public string PrvMessage
        {
            get => _prvMessage ?? string.Empty;
            set { _prvMessage = value; OnPropertyChanged(); }
        }
    }

    public class Discord : OnPropertyChange
    {
        private string _id;

        public string Id
        {
            get => _id ?? string.Empty;
            set { _id = value; OnPropertyChanged(); }
        }

        private string _name;

        public string Name
        {
            get => _name ?? string.Empty;
            set { _name = value; OnPropertyChanged(); }
        }

        private string _keyWord;

        public string KeyWord
        {
            get => _keyWord ?? string.Empty;
            set { _keyWord = value; OnPropertyChanged(); }
        }

        private string _webUrl;

        public string WebHook
        {
            get => _webUrl ?? string.Empty;
            set { _webUrl = value; OnPropertyChanged(); }
        }

        private int _sendMessageEvery;

        public int MessageInterval
        {
            get => _sendMessageEvery;
            set { _sendMessageEvery = value; OnPropertyChanged(); }
        }

        private string _role;
        [XmlIgnore]
        public string Role
        {
            get => _role ?? string.Empty;
            set { _role = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> _roleIds = new ObservableCollection<string>();

        public ObservableCollection<string> RoleIds
        {
            get => _roleIds;
            set { _roleIds = value; OnPropertyChanged(); }
        }
    }

    public class IntervalMessage : OnPropertyChange
    {
        private string _id;

        public string Id
        {
            get => _id ?? string.Empty;
            set { _id = value; OnPropertyChanged(); }
        }

        private string _itemDesc;

        public string ItemDesc
        {
            get => _itemDesc ?? string.Empty;
            set { _itemDesc = value; OnPropertyChanged(); }
        }

        private int _interval;

        public int Interval
        {
            get => _interval;
            set { _interval = value; OnPropertyChanged(); }
        }
    }
}
